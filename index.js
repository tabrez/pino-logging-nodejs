const express = require('express');
const pino = require('pino');
const expressPino = require('express-pino-logger');
const chalk = require('chalk');
const randomId = require('./random-id');

const logger = pino({ 
    level: process.env.LOG_LEVEL || 'info'
// ,    prettyPrint: { colorize: true }
});

const expressLogger = expressPino({ logger });

const PORT = process.env.PORT || 3000;
const app = express();

app.use(expressLogger);

app.get('/', (req, res) => {
    logger.debug(chalk.blue('[App] ') + 'Calling res.send');
    logger.info(chalk.blue('[App/info] ') + 'Calling res.send');
    const id = randomId.getRandomId();
    res.send(`Hello world #${id}`);
});

app.listen(PORT, () => {
    logger.debug(chalk.blue('[App] ') + `Server running on port ${PORT}`);
    logger.info(chalk.yellow('[App/info]') + `Server running on port ${PORT}`);
});
