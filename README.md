# Logging in NodeJS apps

[Tutorial](https://www.twilio.com/blog/guide-node-js-logging)

Specify log level using enviroment variable:
```
LOG_LEVEL=debug node index.js
```

Format pino logs using pino-pretty:
```
LOG_LEVEL=debug node index.js | npx pino-pretty
```

Enable logs of express library when your app is running:
```
DEBUG=express:* node index.js
```

Enable logs of random-id 'library' when your app is running:
```
DEBUG=mylib:randomid node index.js
```

Can combine multiple enviroment variables:
```
LOG_LEVEL=debug DEBUG=mylib:randomid node index.js
```

Show pino logs with timestamp, log level as an emoji and the 
message using pino-colada:

```
node index.js | npx pino-colada
```

Show logs from debug library in pino logs(both formatted by pino-colada):
```
DEBUG=mylib:randomid node -r pino-debug index.js | npx pino-colada
```

Format both pino and debug logs using pino-pretty:
```
DEBUG=mylib:randomid node -r pino-debug index.js | npx pino-pretty
```

Formatting is disabled in debug logs if output is redirected to a file 
(make sure prettyPrint is not enabled in code):
```
LOG_LEVEL=debug DEBUG=mylib:randomid node index.js &> out.log
```

Send all log messages upto debug level of pino to both a file and to standard 
output:
```
LOG_LEVEL=debug node index.js | npx pino-tee debug ./debug.log
```

Send all upto info level of logs to file, all log messages to standard output:
```
LOG_LEVEL=debug node index.js | npx pino-tee info ./info.log
```

In summary,

1. Use pino logs for writing log messages to standard output
2. Use debug library if you want to see logs of a library that your app is 
using and it supports logging
3. Use `-r pino-debug` option to combine pino & debug log messages together(for
formatting, redirecting etc.)
4. Use chalk to format log messages
5. Use pino-tee to send some or all log messages to a file while also sending 
the log messages to standard output
6. Use pino-colada and pino-pretty to format pino logs for easier viewing on 
standard output
7. Do not use pino-colada or pino-pretty or prettyFormat option of pino logger 
in code when redirecting log messages to a file

