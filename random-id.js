const debug = require('debug');
const chalk = require('chalk');

const log = debug('mylib:randomid');

log(chalk.blue('[RI] ') + 'Library loaded');

function getRandomId() {
    log(chalk.blue('[RI] ') + 'Computing random ID');
    const outcome = Math.random()
        .toString(36)
        .substr(2);
    log(chalk.blue('[RI] ') + `Random ID is ${outcome}`);
    return outcome;
}

module.exports = { getRandomId };